import React from 'react';

class Form extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      fields: {},
      errors: {}
    }
  }

  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if(typeof fields["firstname"] !== "undefined"){
      if(!fields["firstname"].match(/^[а-яА-Я]+$/)){
        formIsValid = false;
        errors["firstname"] = " Имя может содержать только буквы";
      }
    }

    if(!fields["firstname"]){
      formIsValid = false;
      errors["firstname"] = " Имя является обязательным полем";
    }

    if(typeof fields["lastname"] !== "undefined"){
      if(!fields["lastname"].match(/^[а-яА-я]+$/)){
        formIsValid = false;
        errors["lastname"] = " Фамилия может содержать только буквы";
      }
    }

    if(!fields["lastname"]){
      formIsValid = false;
      errors["lastname"] = " Фамилия является обязательным полем";
    }

    if(typeof fields["phone"] !== "undefined"){
      if(!fields["phone"].match(/^[0-9]+$/)){
        formIsValid = false;
        errors["phone"] = " Телефон может содержать только цифры";
      }
    }

    if(!fields["phone"]){
      formIsValid = false;
      errors["phone"] = " Номер телефона является обязательным полем";
    }

    if(typeof fields["email"] !== "undefined"){
      let lastAtPos = fields["email"].lastIndexOf('@');
      let lastDotPos = fields["email"].lastIndexOf('.');

      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["email"] = " Email введен некорректно";
      }
    }

    if(!fields["email"]){
      formIsValid = false;
      errors["email"] = " Email является обязательным полем";
    }

    this.setState({errors: errors});
    return formIsValid;
  }

  contactSubmit(e){
    e.preventDefault();

    if(this.handleValidation()){
      alert("Форма отправлена");
    }
    else
    {
      alert("Форма содержит ошибки")
    }

}

handleChange(field, e){
  let fields = this.state.fields;
  fields[field] = e.target.value;
  this.setState({fields});
  }

  render () {
    return (
      <div className='wrapper'>
        <form name="contactform" className="contactform" onSubmit= {this.contactSubmit.bind(this)}>
          <div>
            <fieldset>
              Имя
              <br/>
              <input ref="firstname" type="text" maxLength="254" size="30" placeholder="Иван" onChange={this.handleChange.bind(this, "firstname")} value={this.state.fields["firstname"]}/>
              <span style={{color: "white"}}>{this.state.errors["firstname"]}</span>
              <br/>
              Фамилия
              <br />
              <input ref="lastname" type="text" maxLength="254" size="30" placeholder="Иванов" onChange={this.handleChange.bind(this, "lastname")} value={this.state.fields["lastname"]}/>
              <span style={{color: "white"}}>{this.state.errors["lastname"]}</span>
              <br/>
              Номер телефона
              <br/>
              <input refs="phone" type="text" maxLength="11" size="30" placeholder="79057416010" onChange={this.handleChange.bind(this, "phone")} value={this.state.fields["phone"]}/>
              <span style={{color: "white"}}>{this.state.errors["phone"]}</span>
              <br/>
              Email
              <br/>
              <input refs="email" type="text" size="30" placeholder="ivan.ivanov@gmail.com" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]}/>
              <span style={{color: "white"}}>{this.state.errors["email"]}</span>
              <br/>
              <br/>
              <button type = 'submit'>Продолжить</button>
            </fieldset>
          </div>
        </form>
      </div>
    );
  }
}

export default Form
